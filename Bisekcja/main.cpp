#include <iostream>
#include <cmath>

// ln(x) + x - 5

double abs(double x) {

    return x < 0 ? -x : x;
}

double f(double x) {
    return std::log(x) + x - 5;
}

double findZero(double xMin, double xMax) {

    double mid = (xMin + xMax) / 2.0;
    double value = f(mid);
    if(abs(value) < 10e-5) {
        return mid;
    }

    return value < 0 ? findZero(mid, xMax) : findZero(xMin, mid);
}


int main() {
    std::cout << findZero(0, 10) << std::endl;
    return 0;
}