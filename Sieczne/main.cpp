#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

constexpr double E = 0.000001;

double f(double x) {
    return std::log(x) + x - 5;
}

int main() {

    double x0 = 0;
    double x1 = 5;
    double x2 = 10;
    double f0 = 0;
    double f1 = 0;
    double f2 = 0;

    f1 = f(x1);
    f2 = f(x2);

    size_t iterations = 0;

    while (fabs(x1 - x2) > E) {
        ++iterations;

        if (fabs(f1 - f2) < E) {
            std::cout << "Incorrect start values, change x1 and x2!\n";
            break;
        }

        x0 = x1 - ((f1 * (x1 - x2)) / (f1 - f2));
        f0 = f(x0);

        if (fabs(f0) < E) {
            break;
        }

        x2 = x1;
        f2 = f1;
        x1 = x0;
        f1 = f0;
    }
    std::cout << "x0 = " << x0 << std::endl;
    std::cout << "Iterations: " << iterations << std::endl;
    return 0;
} 