#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

constexpr double E = 0.000001;

double f(double x) {
    return std::log(x) + x - 5;
}


double fp(double x) {
    return (1 / x) + 1;
}

int main() {

    double x0 = 10;
    double x1 = x0 + 1;
    double f0 = f(x0);
    double f1 = 0;

    size_t iterations = 0;

    while ((fabs(x1 - x0) > E) && (fabs(f0) > E)) {
        ++iterations;

        f1 = fp(x0);

        if (fabs(f1) < E) {
            std::cout << "Incorrect starting point, change x0";
            break;
        }

        x1 = x0;
        x0 = x0 - f0 / f1;
        f0 = f(x0);
    }
    std::cout << "x0 = " << x0 << std::endl;
    std::cout << "Iterations = " << iterations << std::endl;

    return 0;
} 