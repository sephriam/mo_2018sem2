import numpy as np

def rosen(x):
    """The Rosenbrock function"""
    return sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0)
    # return x[0]**2.0 + x[1]**2.0

def selectNewPoint(x0, LAMBDA):
    
    for i in range(0,len(x0)):
        x1 = np.copy(x0)
        x1[i] += LAMBDA

        v1 = rosen(x1)
        v2 = rosen(x0)

        x2 = np.copy(x0)
        x2[i] -= LAMBDA
        v3 = rosen(x2)

        if v1 < v3 and v1 < v2:
            x0 = x1
        elif v3 < v1 and v3 < v2:
            x0 = x2

    return x0

def main():

    LAMBDA = 0.1
    ACCURACY = 0.000001
    x0 = np.array([5., 5.])
    steps = 0

    while rosen(x0) > ACCURACY:
        x1 = selectNewPoint(x0, LAMBDA)
        diff = x0 - x1
        if np.array_equal(diff, np.zeros(2)):
            LAMBDA /= 2.0
            continue
        while rosen(x1 - diff) < rosen(x1):
            x1 -= diff
            print(x1, rosen(x1))
            steps += 1
        x0 = x1

    print("STEPS:", steps)

    return

if __name__ == "__main__":
    # execute only if run as a script
    main()
