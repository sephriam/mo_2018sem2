#include <iostream>
#include <cmath>

namespace {
    constexpr double LAMBDA = 1e-5;
    constexpr double DELTA = 1e-8;
}

double sPow(double base, size_t exp) {
    double ret = 1.0;

    for (size_t i = 0; i < exp; ++i) {
        ret *= base;
    }

    return ret;
}

double f(double x, double y) {
    // return sPow(x, 4) - 4 * sPow(x, 2) - 16 + sPow(y, 4) - 4 * sPow(y, 2) - 16;
     // return (x - 1) * (x - 1) + (y - 2) * (y - 2);
    return sPow((1 - x), 2) + 100*sPow((y - sPow(x,2)),2);
}

double fpx(double x, double y) {
    // return 4 * v * (sPow(v, 2) - 2);
     // return 2 * (v - 1);
    return 2*(200 * sPow(x,3) - 200 * x * y + x - 1);
}

double fpy(double x, double y) {
    // return 4 * v * (sPow(v, 2) - 2);
     // return 2 * (v - 2);
    return 200*(y - sPow(x, 2));
}

double steepestDescent(double x, double y) {

    double val1 = f(x, y);
    double val2 = val1 + 2 * DELTA;

    size_t step = 0;

    while (fabs(val1 - val2) > DELTA) {

        size_t minInd = 1;
        double gx = fpx(x,y);
        double gy = fpy(x,y);

        for (size_t i = 1; i < 30; ++i) {
            if (f(x - (i * LAMBDA * gx), y - (i * LAMBDA * gy)) >
                f(x - ((i + 1) * LAMBDA * gx), y - ((i + 1) * LAMBDA * gy))) {
                minInd = i + 1;
            }
        }

        val2 = val1;
        x -= minInd * LAMBDA * gx;
        y -= minInd * LAMBDA * gy;
        val1 = f(x, y);

        std::cout << step << ": " << minInd << " ... " << x << " " << y << " = " << val1 << '\n';
        ++step;
    }

    return val1;
}

int main() {

    std::cout << steepestDescent(5, 5);

    return 0;
}